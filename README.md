Language to Program (Project)

Setup:
Given an sentence [x1, x2, ..., xn], and a initial world state c
the model generates a list of program tokens: [z1, z2, ..., zt], to execute the context state to get the final world state c'. 

Algorithm:

1. initial world representation 
		
		c -> [c0, c1, c2, ..., ck]
		
		
2. contextural sentence embedding,


		[x1,x2, ..., xn] -> SelfAtt-LSTM (Attention on [c0, c1, c2, ..., ck]) -> [h1, h2, h3, ... hn].


		[self attentive language model]



3. Generating Commands:

		given H = [h1, h2, ..., hn] and C = [c0, c1, c2, ..., ck]
	
		s0 = hn, stack_0 = empty
	
		for i = 0...END
		
		[si, stack_i] -> q_i
		
		q_i -> att(H) -> att(C) -> a_i
		
		[q_i, a_i] -> decode token o_i -> append to stack_i execute
		
		si+1 = rnn(si, a_i)


Reference : 
From Language to Programs: Bridging Reinforcement Learning and Maximum Marginal Likelihood 
https://arxiv.org/pdf/1704.07926.pdf

Simpler Context-Dependent Logical Forms via Model Projections
https://arxiv.org/pdf/1606.05378.pdf

